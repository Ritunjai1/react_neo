import React, { Component } from "react";
import "./address.css";
class Del_Add extends Component {
  constructor(props: any) {
    super(props);
  }
  render() {
    return (
      <div>
        <nav className="navbar navbar-default">
          <div className="container">
            <div className="navbar-header">
              <a className="navbar-brand" href="../../index.html">
                <i className="fa fa-shopping-bag fa-lg" aria-hidden="true">
                  {" "}
                  <strong>NeoSTORE</strong>{" "}
                </i>
              </a>
            </div>

            <div className="collapse navbar-collapse">
              <ul className="nav navbar-nav ">
                <li>
                  <a href="./cart.html">Bag</a>
                </li>
                <li className="line" />
                <li>
                  <a href="./address.html">Delivery</a>
                </li>
                <li className="line" />
                <li>
                  <a href="./payment.html">Payment</a>
                </li>
              </ul>

              <ol className="nav navbar-nav navbar-right">
                <li>
                  <a>
                    <i className="fa fa-lock fa-lg" aria-hidden="true" /> 100%
                    SECURE
                  </a>
                </li>
              </ol>
            </div>
          </div>
        </nav>

        <div className="container">
          <div className="panel panel-default">
            <div className="panel-heading">
              <h3>Select delivery address</h3>
              <hr />
            </div>
            <div className="panel-body">
              <div className="panel panel-default">
                <div className="panel-body">
                  <p>IT Sigma Park, Rabale</p>
                  <p>Thane-400614</p>
                  <p>Maharashtra</p>
                  <p>India</p>
                </div>
                <div className="panel-footer">
                  <input type="radio" name="address" /> Select
                  <a
                    href="../user/edit-address.html"
                    type="button"
                    className=" btn-default"
                  >
                    <i className="fa fa-pencil" /> Edit
                  </a>
                </div>
              </div>
            </div>
            <div className="panel-footer">
              <hr />
              <a
                href="./payment.html"
                type="button"
                className="btn btn-default btn-lg"
              >
                {" "}
                Next <i className="fa fa-angle-double-right" />
              </a>
              <a
                href="../user/address.html"
                type="button"
                className="btn btn-default btn-lg"
              >
                {" "}
                Add New <i className="fa fa-plus" />
              </a>
            </div>
          </div>
        </div>

        <div className="footer">
          <div className="container">
            <hr />
            <div className="row text-center">
              <i className="text-info fa fa-3x fa-cc-paypal " />
              <i className="text-danger fa fa-3x fa-cc-mastercard " />
              <i className="text-primary fa fa-3x fa-cc-visa " />
              <i className="text-primary fa fa-3x fa-cc-amex " />
              <i className="text-primary fa fa-3x fa-cc-discover " />
              <i className="text-info fa fa-3x fa-paypal " />
              <i className="text-danger fa fa-3x fa-google-wallet " />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Del_Add;
