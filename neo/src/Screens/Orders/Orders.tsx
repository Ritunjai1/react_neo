import React, { Component } from "react";
import "./Orders.css";
import { Router, Link } from "@reach/router";

class Orders extends Component {
  constructor(props: any) {
    super(props);
  }
  render() {
    return (
      <div className="container">
        <div className="container">
          <div className="container-fluid">
            <div className="col-md-12">
              <h4> My Account </h4>
              <hr />
            </div>

            <div className="col-md-3">
              <img
                src="http://soeasyloansonline.com.au/img/testimonial/noimg.png"
                className="img-responsive img-circle profile"
                alt="profile_pic"
              />

              <div className="text-center">
                <br />
                <div className="text-danger">
                  <strong>Test User</strong>
                </div>
              </div>
              <br />
              <ul className="nav">
                <li>
                  <Link to="#">
                    <i className="fa fa-pencil fa-fw" aria-hidden="true" />
                    &nbsp; Orders
                  </Link>
                </li>
                <li>
                  <Link to="/Profile">
                    <i className="fa fa-user fa-fw" aria-hidden="true" />
                    &nbsp; Profile
                  </Link>
                </li>
                <li>
                  <Link to="/Address">
                    <i
                      className="fa fa-address-book fa-fw"
                      aria-hidden="true"
                    />
                    &nbsp; Address
                  </Link>
                </li>
              </ul>
            </div>

            <div className="col-md-9">
              <div className="panel panel-default">
                <div className="panel-heading text-muted">
                  <span className="text-warning">
                    <strong>TRANSIT</strong>
                  </span>
                  <span>Order No: 5sd4a442341ds</span>
                  <div>
                    <small>Placed on 15/09/2017 / </small>
                    <small className="text-success">
                      <strong>2000$</strong>
                    </small>
                  </div>
                  <hr />
                </div>
                <div className="panel-body">
                  <div className="row">
                    <div className="col-md-3">
                      <div className="thumbnail">
                        <img
                          src="http://via.placeholder.com/150x100"
                          alt="product_image"
                          className="img-rounded"
                        />
                      </div>
                    </div>
                    <div className="col-md-3">
                      <div className="thumbnail">
                        <img
                          src="http://via.placeholder.com/150x100"
                          alt="product_image"
                          className="img-rounded"
                        />
                      </div>
                    </div>
                    <div className="col-md-3">
                      <div className="thumbnail">
                        <img
                          src="http://via.placeholder.com/150x100"
                          alt="product_image"
                          className="img-rounded"
                        />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="panel-footer">
                  <button type="button" className="btn btn-success">
                    Download invoice as PDF
                  </button>
                </div>
              </div>
              <div className="panel panel-default">
                <div className="panel-heading text-muted">
                  <span className="text-warning">
                    <strong>TRANSIT</strong>
                  </span>
                  <span>Order No: 5sd4a442341ds</span>
                  <div>
                    <small>Placed on 15/09/2017 / </small>
                    <small className="text-success">
                      <strong>2000$</strong>
                    </small>
                  </div>
                  <hr />
                </div>
                <div className="panel-body">
                  <div className="row">
                    <div className="col-md-3">
                      <div className="thumbnail">
                        <img
                          src="http://via.placeholder.com/150x100"
                          alt="product_image"
                          className="img-rounded"
                        />
                      </div>
                    </div>
                    <div className="col-md-3">
                      <div className="thumbnail">
                        <img
                          src="http://via.placeholder.com/150x100"
                          alt="product_image"
                          className="img-rounded"
                        />
                      </div>
                    </div>
                    <div className="col-md-3">
                      <div className="thumbnail">
                        <img
                          src="http://via.placeholder.com/150x100"
                          alt="product_image"
                          className="img-rounded"
                        />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="panel-footer">
                  <button type="button" className="btn btn-success">
                    Download invoice as PDF
                  </button>
                </div>
              </div>

              <hr />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Orders;
