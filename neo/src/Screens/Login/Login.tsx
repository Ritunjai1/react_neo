import React, { Component } from "react";
import "./Login.css";
import { Formik } from "formik";
import * as Yup from "yup";

class Login extends Component {
  constructor(props: any) {
    super(props);
  }
  render() {
    return (
      <div className="container">
        <div className="container-login">
          <div className="col-md-5">
            <div className="panel panel-default">
              <div className="panel-heading">
                <h2 className="text-center text-muted">Login to NeoSTORE</h2>
              </div>
              <div className="panel-body">
                <p className="text-muted text-center">EASILY USING</p>
                <button className="btn btn-default btn-lg">
                  <i className="fa fa-facebook fa-lg  text-primary" />
                  Facebook
                </button>
                <button className="btn btn-default btn-lg pull-right">
                  <i className="fa fa-google fa-lg text-danger" />
                  Google
                </button>

                <p className="text-muted text-center">--OR USING--</p>
                <Formik
                  initialValues={{ email: "", password: "" }}
                  onSubmit={(values, { setSubmitting }) => {
                    setTimeout(() => {
                      alert(JSON.stringify(values, null, 2));
                      setSubmitting(false);
                    }, 500);
                  }}
                  validationSchema={Yup.object().shape({
                    email: Yup.string()
                      .email()
                      .required("Required"),
                    password: Yup.string()
                      .matches(
                        /^(?=.*[a-zA-Z])(?=.*[0-9])/,
                        "Must be one upper,lower and numeric and special char"
                      )
                      .required("Required")
                  })}
                >
                  {props => {
                    const {
                      values,
                      touched,
                      errors,
                      dirty,
                      isSubmitting,
                      handleChange,
                      handleBlur,
                      handleSubmit,
                      handleReset
                    } = props;
                    return (
                      <form className="form-custom" onSubmit={handleSubmit}>
                        <div className="form-group">
                          <input
                            type="email"
                            className={
                              errors.email && touched.email
                                ? "form-control error"
                                : "form-control"
                            }
                            placeholder="Email Address"
                            name="email"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.email}
                          />
                          <small className="text-danger">
                            {errors.email && touched.email && (
                              <div className="input-feedback">
                                {errors.email}
                              </div>
                            )}
                          </small>

                          <input
                            type="password"
                            className={
                              errors.password && touched.password
                                ? "form-control error"
                                : "form-control"
                            }
                            placeholder="Password"
                            name="password"
                            onBlur={handleBlur}
                            onChange={handleChange}
                          />
                          <small className="text-danger">
                            {errors.password && touched.password && (
                              <div className="input-feedback">
                                {errors.password}
                              </div>
                            )}
                          </small>
                        </div>
                        <div className="form-group">
                          <button
                            className="btn btn-lg btn-primary btn-block"
                            type="submit"
                          >
                            Login
                          </button>
                        </div>
                      </form>
                    );
                  }}
                </Formik>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Login;
