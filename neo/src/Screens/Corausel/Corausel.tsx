import React, { Component } from "react";
import "./Corausel.css";
import { Router, Link } from "@reach/router";
import "react-alice-carousel/lib/alice-carousel.css";
import AliceCarousel from "react-alice-carousel";
class Corausel extends Component {
  constructor(props: any) {
    super(props);
  }
  public render() {
    return (
      <div className="container">
        <div className="row">
          <div className="carousel-holder">
            <div
              id="carousel-example-generic"
              className="carousel slide"
              data-ride="carousel"
            >
              <ol className="carousel-indicators">
                <li
                  data-target="#carousel-example-generic"
                  data-slide-to="0"
                  className="active"
                />
                <li data-target="#carousel-example-generic" data-slide-to="i" />
              </ol>
              <div className="carousel-inner" role="listbox">
                <div className="item active">
                  <img
                    className="slide-image"
                    src="https://images-na.ssl-images-amazon.com/images/I/51hOIrUnGmL._SX425_.jpg"
                    alt=""
                  />
                  <div className="carousel-caption">
                    <h4>Lorem Ipsum</h4>
                  </div>
                </div>
                <div className="item">
                  <img
                    className="slide-image"
                    src="https://images-na.ssl-images-amazon.com/images/I/51hOIrUnGmL._SX425_.jpg"
                    alt=""
                  />
                  <div className="carousel-caption">
                    <h4>Lorem Ipsum</h4>
                  </div>
                </div>
                <div className="item">
                  <img
                    className="slide-image"
                    src="https://images-na.ssl-images-amazon.com/images/I/51hOIrUnGmL._SX425_.jpg"
                    alt=""
                  />
                  <div className="carousel-caption">
                    <h4>Lorem Ipsum</h4>
                  </div>
                </div>
              </div>
              <a
                className="left carousel-control"
                href="#carousel-example-generic"
                data-slide="prev"
              >
                <span className="glyphicon glyphicon-chevron-left" />
              </a>
              <a
                className="right carousel-control"
                href="#carousel-example-generic"
                data-slide="next"
              >
                <span className="glyphicon glyphicon-chevron-right" />
              </a>
            </div>
          </div>
          {/* <!--<div className="new">--> */}
        </div>
        <div className="container">
          <div className="row">
            <div className="container">
              <h3 className="text-center">
                Popular Products{" "}
                <small>
                  <Link to="Product_list">--View all</Link>
                </small>
              </h3>
              <div className="row">
                <div className="col-md-4">
                  <div className="thumbnail">
                    <div className="img-thumb">
                      <img
                        src="http://via.placeholder.com/300x200"
                        className="img img-responsive"
                      />
                    </div>
                    <div className="caption">
                      <h4 className="text-center">
                        <Link to="Pro_details">Sed ut perspiciatis</Link>
                      </h4>
                      <h4 className="text-center">20000 $</h4>
                      <div className="text-center">
                        <i className="fa fa-lg fa-star" />
                        <i className="fa fa-lg fa-star" />
                        <i className="fa fa-lg fa-star" />
                        <i className="fa fa-lg fa-star-o" />
                        <i className="fa fa-lg fa-star-o" />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-md-4">
                  <div className="thumbnail">
                    <div className="img-thumb">
                      <img
                        src="http://via.placeholder.com/300x200"
                        className="img img-responsive"
                      />
                    </div>
                    <div className="caption">
                      <h4 className="text-center">
                        <Link to="Pro_details">Sed ut perspiciatis</Link>
                      </h4>
                      <h4 className="text-center">20000 $</h4>
                      <div className="text-center">
                        <i className="fa fa-lg fa-star" />
                        <i className="fa fa-lg fa-star" />
                        <i className="fa fa-lg fa-star" />
                        <i className="fa fa-lg fa-star-o" />
                        <i className="fa fa-lg fa-star-o" />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-md-4">
                  <div className="thumbnail">
                    <div className="img-thumb">
                      <img
                        src="http://via.placeholder.com/300x200"
                        className="img img-responsive"
                      />
                    </div>
                    <div className="caption">
                      <h4 className="text-center">
                        <Link to="Pro_details">Sed ut perspiciatis</Link>
                      </h4>
                      <h4 className="text-center">20000 $</h4>
                      <div className="text-center">
                        <i className="fa fa-lg fa-star" />
                        <i className="fa fa-lg fa-star" />
                        <i className="fa fa-lg fa-star" />
                        <i className="fa fa-lg fa-star-o" />
                        <i className="fa fa-lg fa-star-o" />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Corausel;
