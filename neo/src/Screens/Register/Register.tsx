import React, { Component } from "react";
import "./Register.css";
import { Formik } from "formik";
import * as Yup from "yup";
import axios from "axios";
import { ToastsContainer, ToastsStore } from "react-toasts";

interface IProps {
  email: string;
  password: string;
  phone_no: string;
}
interface IState {
  [email: string]: any;
  password: string;
  phone_no: string;
}

class Register extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      email: "",
      password: "",
      phone_no: ""
    };
  }
  OnChange = (e: any) => {
    e.preventDefault();
    const { name, value } = e.target;
    console.log(value);
    this.setState({
      [name]: value
    });
  };
  render() {
    return (
      <div className="container">
        <div className="container-register">
          <div className="col-md-5">
            <div className="panel panel-default">
              <div className="panel-heading">
                <h2 className="text-center text-muted">Register to NeoSTORE</h2>
              </div>
              <div className="panel-body">
                <p className="text-muted text-center">EASILY USING</p>
                <button className="btn btn-default btn-lg">
                  <i className="fa fa-facebook fa-lg  text-primary" />
                  Facebook
                </button>
                <button className="btn btn-default btn-lg pull-right">
                  <i className="fa fa-google fa-lg text-danger" />
                  Google
                </button>

                <p className="text-muted text-center">--OR USING--</p>

                <Formik
                  initialValues={{
                    email: "",
                    password: "",
                    confirm: "",
                    phone: ""
                  }}
                  onSubmit={(values, { setSubmitting }) => {
                    // setTimeout(() => {
                    //   alert(JSON.stringify(values, null, 2));
                    //   setSubmitting(false);
                    // }, 500);
                    ToastsStore.success("Hurray! User is Registered");

                    const { email, password, confirm, phone } = this.state;
                    axios
                      .post("", { email, password, confirm, phone })
                      .then(response => {});
                  }}
                  validationSchema={Yup.object().shape({
                    email: Yup.string()
                      .email()
                      .required("Required"),
                    password: Yup.string()
                      .matches(
                        /^(?=.*[a-zA-Z])(?=.*[0-9])/,
                        "Must be one upper,lower and numeric and special char"
                      )
                      .min(8, "Please enter more than 8 characters")
                      .required("Required"),
                    confirm: Yup.string()
                      .oneOf([Yup.ref("password")], "Must be same as password")
                      .required("Required"),
                    phone: Yup.string()
                      .matches(/^[6-9]\d{9}$/, "Must be 10 digits")
                      .required("Required")
                  })}
                >
                  {props => {
                    const {
                      values,
                      touched,
                      errors,
                      dirty,
                      isSubmitting,
                      handleChange,
                      handleBlur,
                      handleSubmit,
                      handleReset
                    } = props;
                    return (
                      <form className="form-custom" onSubmit={handleSubmit}>
                        <div className="form-group">
                          <input
                            type="email"
                            className={
                              errors.email && touched.email
                                ? "form-control error"
                                : "form-control"
                            }
                            placeholder="Your Email Address"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            name="email"
                            value={values.email}
                          />
                          <small className="text-danger">
                            {errors.email && touched.email && (
                              <div className="input-feedback">
                                {errors.email}
                              </div>
                            )}
                          </small>

                          <input
                            type="password"
                            className={
                              errors.password && touched.password
                                ? "form-control error"
                                : "form-control"
                            }
                            placeholder="Choose Password"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.password}
                            name="password"
                          />
                          <small className="text-danger">
                            {errors.password && touched.password && (
                              <div className="input-feedback">
                                {errors.password}
                              </div>
                            )}
                          </small>

                          <input
                            type="password"
                            className={
                              errors.confirm && touched.confirm
                                ? "form-control error"
                                : "form-control"
                            }
                            placeholder="Confirm Password"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.confirm}
                            name="confirm"
                          />
                          <small className="text-danger">
                            {errors.confirm && touched.confirm && (
                              <div className="input-feedback">
                                {errors.confirm}
                              </div>
                            )}
                          </small>

                          <input
                            type="number"
                            className={
                              errors.confirm && touched.confirm
                                ? "form-control error"
                                : "form-control"
                            }
                            placeholder="Enter Phone Number"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            name="phone"
                            value={values.phone}
                          />
                          <small className="text-danger">
                            {errors.phone && touched.phone && (
                              <div className="input-feedback">
                                {errors.phone}
                              </div>
                            )}
                          </small>

                          <legend className="gender-legend">I'm</legend>
                          <div className="checkbox">
                            <label>
                              <input
                                type="radio"
                                value="male"
                                name="gender"
                                checked={this.state.selected === "male"}
                                onChange={e =>
                                  this.setState({ selected: e.target.value })
                                }
                              />{" "}
                              <strong>Male</strong>{" "}
                            </label>
                            <label>
                              <input
                                type="radio"
                                value="female"
                                name="gender"
                                checked={this.state.selected === "female"}
                                onChange={e =>
                                  this.setState({ selected: e.target.value })
                                }
                              />{" "}
                              <strong>Female</strong>{" "}
                            </label>
                          </div>
                          {/* <small className="text-danger">Please select</small> */}

                          <button
                            type="submit"
                            className="btn btn-lg btn-primary btn-block"
                          >
                            Register
                          </button>
                        </div>
                      </form>
                    );
                  }}
                </Formik>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Register;
