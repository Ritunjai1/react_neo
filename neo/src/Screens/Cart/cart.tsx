import React, { Component } from "react";
import "../Cart/cart.css";
import { Router, Link } from "@reach/router";
class Cart extends Component {
  constructor(props: any) {
    super(props);
  }
  render() {
    return (
      <div>
        <nav className="navbar navbar-default">
          <div className="container">
            <div className="navbar-header">
              <Link to="/" className="navbar-brand">
                <i className="fa fa-shopping-bag fa-lg" aria-hidden="true">
                  {" "}
                  <strong>NeoSTORE</strong>{" "}
                </i>
              </Link>
            </div>

            <div className="collapse navbar-collapse">
              <ul className="nav navbar-nav ">
                <li>
                  <a href="./cart.html">Bag</a>
                </li>
                <li className="line" />
                <li>
                  <a href="./address.html">Delivery</a>
                </li>
                <li className="line" />
                <li>
                  <a href="./payment.html">Payment</a>
                </li>
              </ul>

              <ol className="nav navbar-nav navbar-right">
                <li>
                  <a>
                    <i className="fa fa-lock fa-lg" aria-hidden="true" /> 100%
                    SECURE
                  </a>
                </li>
              </ol>
            </div>
          </div>
        </nav>
        <div className="container">
          <div className="row">
            <hr />
            <div className="col-md-8">
              <table className="table table-hover">
                <thead>
                  <tr>
                    <th>Product</th>
                    <th className="text-center">Quantity</th>
                    <th className="text-center">Price</th>
                    <th className="text-center">Total</th>
                    <th> </th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td className="col-sm-8 col-md-6">
                      <div className="media">
                        <div className="media-left">
                          <a>
                            <img
                              className="media-object cover"
                              src="http://via.placeholder.com/72x72"
                            />
                          </a>
                        </div>

                        <div className="media-body">
                          <h4 className="media-heading">
                            <a>Voluptatem</a>
                          </h4>
                          <h5 className="media-heading">
                            {" "}
                            by{" "}
                            <a>
                              <small>Laudantium</small>
                            </a>
                          </h5>
                          <span>Status: </span>
                          <span className="text-success">
                            <strong>In Stock</strong>
                          </span>
                        </div>
                      </div>
                    </td>
                    <td className="col-sm-1 col-md-2 text-center">
                      <i className="fa fa-minus-circle" />
                      <input
                        type="number"
                        value="2"
                        disabled
                        className="text-center quantity"
                      />
                      <i className="fa fa-plus-circle" />
                    </td>
                    <td className="col-sm-1 col-md-1 text-center">
                      <strong>2000$</strong>
                    </td>
                    <td className="col-sm-1 col-md-1 text-center">
                      <strong>4000$</strong>
                    </td>
                    <td className="col-sm-1 col-md-1">
                      <button type="button" className="btn btn-sm btn-danger">
                        <i className="fa fa-trash" />
                      </button>
                    </td>
                  </tr>
                  <tr>
                    <td className="col-sm-8 col-md-6">
                      <div className="media">
                        <div className="media-left">
                          <a>
                            <img
                              className="media-object cover"
                              src="http://via.placeholder.com/72x72"
                            />
                          </a>
                        </div>

                        <div className="media-body">
                          <h4 className="media-heading">
                            <a>Voluptatem</a>
                          </h4>
                          <h5 className="media-heading">
                            {" "}
                            by{" "}
                            <a>
                              <small>Laudantium</small>
                            </a>
                          </h5>
                          <span>Status: </span>
                          <span className="text-success">
                            <strong>In Stock</strong>
                          </span>
                        </div>
                      </div>
                    </td>
                    <td className="col-sm-1 col-md-2 text-center">
                      <i className="fa fa-minus-circle" />
                      <input
                        type="number"
                        value="2"
                        disabled
                        className="text-center quantity"
                      />
                      <i className="fa fa-plus-circle" />
                    </td>
                    <td className="col-sm-1 col-md-1 text-center">
                      <strong>2000$</strong>
                    </td>
                    <td className="col-sm-1 col-md-1 text-center">
                      <strong>4000$</strong>
                    </td>
                    <td className="col-sm-1 col-md-1">
                      <button type="button" className="btn btn-sm btn-danger">
                        <i className="fa fa-trash" />
                      </button>
                    </td>
                  </tr>
                  <tr>
                    <td className="col-sm-8 col-md-6">
                      <div className="media">
                        <div className="media-left">
                          <a>
                            <img
                              className="media-object cover"
                              src="http://via.placeholder.com/72x72"
                            />
                          </a>
                        </div>

                        <div className="media-body">
                          <h4 className="media-heading">
                            <a>Voluptatem</a>
                          </h4>
                          <h5 className="media-heading">
                            {" "}
                            by{" "}
                            <a>
                              <small>Laudantium</small>
                            </a>
                          </h5>
                          <span>Status: </span>
                          <span className="text-success">
                            <strong>In Stock</strong>
                          </span>
                        </div>
                      </div>
                    </td>
                    <td className="col-sm-1 col-md-2 text-center">
                      <i className="fa fa-minus-circle" />
                      <input
                        type="number"
                        value="2"
                        disabled
                        className="text-center quantity"
                      />
                      <i className="fa fa-plus-circle" />
                    </td>
                    <td className="col-sm-1 col-md-1 text-center">
                      <strong>2000$</strong>
                    </td>
                    <td className="col-sm-1 col-md-1 text-center">
                      <strong>4000$</strong>
                    </td>
                    <td className="col-sm-1 col-md-1">
                      <button type="button" className="btn btn-sm btn-danger">
                        <i className="fa fa-trash" />
                      </button>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>

            <div className="col-md-4">
              <div className="panel panel-default">
                <div className="panel-heading text-center panel-heading-custom">
                  <h4>Review Order</h4>
                </div>
                <div className="panel-body">
                  <div className="col-md-12">
                    <strong>Subtotal (# items 3)</strong>
                    <div className="pull-right">
                      <span>12000 $</span>
                    </div>
                    <hr />
                  </div>
                  <div className="col-md-12">
                    <strong>GST(5%)</strong>
                    <div className="pull-right">
                      <span>600 $</span>
                    </div>
                    <hr />
                  </div>
                  <div className="col-md-12">
                    <strong>Order Total</strong>
                    <div className="pull-right">
                      <span>12600 $</span>
                    </div>
                    <hr />
                  </div>
                </div>
                <div className="panel-footer">
                  <Link
                    to="/del_address"
                    type="button"
                    className="btn btn-primary btn-lg btn-block"
                  >
                    Checkout
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Cart;
