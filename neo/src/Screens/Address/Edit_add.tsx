import React, { Component } from "react";
import "./Edit_add.css";
class EditAdd extends Component {
  render() {
    return (
      <div>
        <div className="container">
          <div className="container">
            <div className="container-fluid">
              <div className="col-md-12">
                <h4> My Account </h4>
                <hr />
              </div>

              <div className="col-md-3">
                <img
                  src="http://soeasyloansonline.com.au/img/testimonial/noimg.png"
                  className="img-responsive img-circle profile"
                  alt="profile_pic"
                />

                <div className="text-center">
                  <br />
                  <div className="text-danger">
                    <strong>Test User</strong>
                  </div>
                </div>
                <br />
                <ul className="nav">
                  <li>
                    <a href="./orders.html">
                      <i className="fa fa-pencil fa-fw" aria-hidden="true" />
                      &nbsp; Orders
                    </a>
                  </li>
                  <li>
                    <a href="./profile.html">
                      <i className="fa fa-user fa-fw" aria-hidden="true" />
                      &nbsp; Profile
                    </a>
                  </li>
                  <li>
                    <a href="./address.html">
                      <i
                        className="fa fa-address-book fa-fw"
                        aria-hidden="true"
                      />
                      &nbsp; Address
                    </a>
                  </li>
                </ul>
              </div>

              <div className="col-md-9">
                <div className="panel panel-default">
                  <div className="panel-body">
                    <h4>
                      <strong>Add new address</strong>
                    </h4>
                    <hr />

                    <button type="button" className="btn btn-default">
                      <i className="fa fa-map-marker" /> Set Current Location
                    </button>
                    <br />
                    <br />
                    <form>
                      <div className="row">
                        <div className="col-md-8">
                          <div className="form-group">
                            <label>Address</label>
                            <textarea className="form-control">
                              IT Sigma Park, Rabale
                            </textarea>
                          </div>
                          <small className="text-danger">
                            Please enter valid address
                          </small>
                        </div>
                      </div>

                      <div className="row">
                        <div className="col-md-4">
                          <div className="form-group">
                            <label>Pincode</label>
                            <input
                              type="number"
                              className="form-control"
                              value="400614"
                            />
                          </div>
                          <small className="text-danger">
                            Please enter pincode
                          </small>
                        </div>
                      </div>

                      <div className="row">
                        <div className="col-md-4">
                          <div className="form-group">
                            <label>City</label>
                            <input
                              type="text"
                              className="form-control"
                              value="Thane"
                            />
                          </div>
                          <small className="text-danger">
                            Please enter valid city
                          </small>
                        </div>
                      </div>

                      <div className="row">
                        <div className="col-md-4">
                          <div className="form-group">
                            <label>State</label>
                            <input
                              type="text"
                              className="form-control"
                              value="Maharashtra"
                            />
                          </div>
                          <small className="text-danger">
                            Please enter valid state
                          </small>
                        </div>
                      </div>

                      <div className="row">
                        <div className="col-md-4">
                          <div className="form-group">
                            <label>Country</label>
                            <input
                              type="text"
                              className="form-control"
                              value="India"
                            />
                          </div>
                          <small className="text-danger">
                            Please enter valid country
                          </small>
                        </div>
                      </div>

                      <hr />
                      <a type="submit" className="btn btn-default btn-lg">
                        <i className="fa fa-floppy-o" />
                        Save
                      </a>
                      <a
                        href="./address.html"
                        type="button"
                        className="btn btn-default btn-lg"
                      >
                        <i className="fa fa-remove" />
                        Cancel
                      </a>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default EditAdd;
