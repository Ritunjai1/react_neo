import React, { Component } from "react";
import "./Address.css";
import { Router, Link } from "@reach/router";

class Address extends Component {
  constructor(props: any) {
    super(props);
  }
  render() {
    return (
      <div className="container">
        <div className="container">
          <div className="container-fluid">
            <div className="col-md-12">
              <h4> My Account </h4>
              <hr />
            </div>

            <div className="col-md-3">
              <img
                src="http://soeasyloansonline.com.au/img/testimonial/noimg.png"
                className="img-responsive img-circle profile"
                alt="profile_pic"
              />

              <div className="text-center">
                <br />
                <div className="text-danger">
                  <strong>Test User</strong>
                </div>
              </div>
              <br />
              <ul className="nav">
                <li>
                  <Link to="/Orders">
                    <i className="fa fa-pencil fa-fw" aria-hidden="true" />
                    &nbsp; Orders
                  </Link>
                </li>
                <li>
                  <Link to="/Profile">
                    <i className="fa fa-user fa-fw" aria-hidden="true" />
                    &nbsp; Profile
                  </Link>
                </li>
                <li>
                  <Link to="#">
                    <i
                      className="fa fa-address-book fa-fw"
                      aria-hidden="true"
                    />
                    &nbsp; Address
                  </Link>
                </li>
              </ul>
            </div>

            <div className="col-md-9">
              <div className="panel panel-default">
                <div className="panel-heading">
                  <h3>Addresses</h3>
                  <hr />
                </div>
                <div className="panel-body">
                  <div className="panel panel-default">
                    <div className="panel-body">
                      <p>IT Sigma Park, Rabale</p>
                      <p>Thane-400614</p>
                      <p>Maharashtra</p>
                      <p>India</p>
                    </div>
                    <div className="panel-footer">
                      <input type="radio" name="address" /> Select
                      <Link
                        to="/edit_add"
                        type="button"
                        className=" btn-default"
                      >
                        <i className="fa fa-pencil" /> Edit
                      </Link>
                    </div>
                  </div>
                </div>
                <div className="panel-footer">
                  <hr />
                  <a
                    href="./add-address.html"
                    type="button"
                    className="btn btn-default btn-lg"
                  >
                    Add new
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Address;
