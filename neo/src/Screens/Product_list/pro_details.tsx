import React, { Component } from "react";
import "./pro_details.css";
import { Router, Link } from "@reach/router";
class Details extends Component {
  constructor(props: any) {
    super(props);
  }
  render() {
    return (
      <div className="container">
        <div className="card">
          <div className="row">
            <div className="wrapper">
              <div className="col-md-6">
                <div className="preview">
                  <div className="preview-pic tab-content">
                    <div className="my-img active">
                      <img
                        className="actual-img"
                        src="http://via.placeholder.com/300x200"
                      />
                    </div>
                  </div>
                  <ul className="preview-thumbnail nav nav-tabs">
                    <li>
                      <div className="my-img-thumb">
                        <img src="http://via.placeholder.com/100x50" />
                      </div>
                    </li>
                    <li>
                      <div className="my-img-thumb">
                        <img src="http://via.placeholder.com/100x50" />
                      </div>
                    </li>
                    <li>
                      <div className="my-img-thumb">
                        <img src="http://via.placeholder.com/100x50" />
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="col-md-6 ">
                <div className="details">
                  <h3 className="text-danger">At vero eos et </h3>

                  <div className="text-warning">
                    <i className="fa fa-lg fa-star" />
                    <i className="fa fa-lg fa-star" />
                    <i className="fa fa-lg fa-star" />
                    <i className="fa fa-lg fa-star-o" />
                    <i className="fa fa-lg fa-star-o" />
                  </div>

                  <hr />
                  <h4>
                    Price: <span className="text-success">20000 $</span>
                  </h4>
                  <h4>
                    Color:
                    <span className="color" />
                  </h4>

                  <div className="action">
                    <h4>
                      Share on
                      <i className="fa fa-share-alt fa-lg" />
                    </h4>
                    <div className="share-container">
                      <a href="#" className="btn btn-primary">
                        <i className="fa fa-lg fa-facebook" />
                      </a>
                      &nbsp;
                      <a href="#" className="btn btn-danger">
                        <i className="fa fa-lg fa-google" />
                      </a>
                      &nbsp;
                      <a href="#" className="btn btn-info">
                        <i className="fa fa-lg fa-twitter" />
                      </a>
                      &nbsp;
                      <a href="#" className="btn btn-primary">
                        <i className="fa fa-lg fa-linkedin" />
                      </a>
                      &nbsp;
                      <a href="#" className="btn btn-success">
                        <i className="fa fa-lg fa-whatsapp" />
                      </a>
                    </div>
                  </div>

                  <div className="action">
                    <button className="btn btn-primary" type="button">
                      ADD TO CART
                    </button>
                    <button
                      type="button"
                      className="btn btn-warning"
                      data-toggle="modal"
                      data-target=".bs-example-modal-sm"
                    >
                      RATE PRODUCT
                    </button>

                    <div
                      className="modal fade bs-example-modal-sm"
                      role="dialog"
                      aria-labelledby="mySmallModalLabel"
                    >
                      <div className="modal-dialog modal-sm" role="document">
                        <div className="modal-content">
                          <div className="modal-body">
                            <div className="text-warning text-center">
                              <i className="fa fa-lg fa-star" />
                              <i className="fa fa-lg fa-star" />
                              <i className="fa fa-lg fa-star" />
                              <i className="fa fa-lg fa-star-o" />
                              <i className="fa fa-lg fa-star-o" />
                            </div>
                          </div>
                          <div className="modal-footer">
                            <button
                              type="button"
                              className="btn btn-default"
                              data-dismiss="modal"
                            >
                              CLOSE
                            </button>
                            <button type="button" className="btn btn-primary">
                              RATE IT
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <hr />
            <div className="wrapper">
              <div className="col-md-12">
                <div className="preview">
                  <div>
                    <ul className="nav nav-tabs" role="tablist">
                      <li role="presentation" className="active">
                        <a
                          href="#tab1"
                          aria-controls="tab1"
                          role="tab"
                          data-toggle="tab"
                        >
                          Tab 1
                        </a>
                      </li>
                      <li role="presentation">
                        <a
                          href="#tab2"
                          aria-controls="tab2"
                          role="tab"
                          data-toggle="tab"
                        >
                          Tab 2
                        </a>
                      </li>
                      <li role="presentation">
                        <a
                          href="#tab3"
                          aria-controls="tab3"
                          role="tab"
                          data-toggle="tab"
                        >
                          Tab 3
                        </a>
                      </li>
                    </ul>
                    <br />
                    <div className="tab-content">
                      <div
                        role="tabpanel"
                        className="tab-pane active"
                        id="tab1"
                      >
                        Contrary to popular belief, Lorem Ipsum is not simply
                        random text
                      </div>
                      <div role="tabpanel" className="tab-pane" id="tab2">
                        It has roots in a piece of classNameical Latin
                        literature from 45 BC,{" "}
                      </div>
                      <div role="tabpanel" className="tab-pane" id="tab3">
                        making it over 2000 years old. Richard McClintock, a
                        Latin{" "}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Details;
