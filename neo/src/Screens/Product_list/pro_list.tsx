import React, { Component } from "react";
import "./pro_list.css";
import { Router, Link } from "@reach/router";
class List extends Component {
  constructor(props: any) {
    super(props);
  }
  render() {
    return (
      <div className="container">
        <div className="row">
          {/* <!-- Sidebar --> */}
          <div className="col-md-3">
            <div
              className="panel-group"
              id="accordion"
              role="tablist"
              aria-multiselectable="true"
            >
              <div className="panel panel-danger">
                <div className="panel-heading" role="tab" id="headingOne">
                  <h4 className="panel-title">
                    <a
                      data-toggle="collapse"
                      data-parent="#accordion"
                      href="#categories"
                    >
                      <i className="fa fa-lg fa-angle-double-down" /> Categories
                    </a>
                  </h4>
                </div>
                <div
                  id="categories"
                  className="panel-collapse collapse in"
                  role="tabpanel"
                  aria-labelledby="headingOne"
                >
                  <div className="list-group">
                    <li className="list-group-item">
                      <a>
                        <i className="fa fa-dot-circle-o" /> Consectetur
                      </a>
                    </li>
                    <li className="list-group-item">
                      <a>
                        <i className="fa fa-dot-circle-o" /> Adipiscing
                      </a>
                    </li>
                    <li className="list-group-item">
                      <a>
                        <i className="fa fa-dot-circle-o" /> Eiusmod
                      </a>
                    </li>
                    <li className="list-group-item">
                      <a>
                        <i className="fa fa-dot-circle-o" /> Incididunt
                      </a>
                    </li>
                  </div>
                </div>
              </div>

              <div className="panel panel-danger">
                <div className="panel-heading" role="tab" id="headingOne">
                  <h4 className="panel-title">
                    <a
                      data-toggle="collapse"
                      data-parent="#accordion"
                      href="#colors"
                    >
                      <i className="fa fa-lg fa-angle-double-down" /> Colors
                    </a>
                  </h4>
                </div>
                <div
                  id="colors"
                  className="panel-collapse collapse in"
                  role="tabpanel"
                  aria-labelledby="headingOne"
                >
                  <div className="list-group-item">
                    <ul className="list-inline">
                      <li>
                        <button
                          type="button"
                          data-toggle="tooltip"
                          data-placement="top"
                          title="color"
                          className="color-box"
                        />
                      </li>
                      <li>
                        <button
                          type="button"
                          data-toggle="tooltip"
                          data-placement="top"
                          title="color"
                          className="color-box"
                        />
                      </li>
                      <li>
                        <button
                          type="button"
                          data-toggle="tooltip"
                          data-placement="top"
                          title="color"
                          className="color-box"
                        />
                      </li>
                      <li>
                        <button
                          type="button"
                          data-toggle="tooltip"
                          data-placement="top"
                          title="color"
                          className="color-box"
                        />
                      </li>
                      <li>
                        <button
                          type="button"
                          data-toggle="tooltip"
                          data-placement="top"
                          title="color"
                          className="color-box"
                        />
                      </li>
                      <li>
                        <button
                          type="button"
                          data-toggle="tooltip"
                          data-placement="top"
                          title="color"
                          className="color-box"
                        />
                      </li>
                      <li>
                        <button
                          type="button"
                          data-toggle="tooltip"
                          data-placement="top"
                          title="color"
                          className="color-box"
                        />
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>

          {/* <!-- Product list --> */}
          <div className="col-md-9 vertical-line">
            <div className="row padding-row">
              <h5 className="pull-left">Lorem Ipsum</h5>
              <div className="pull-right">
                <ul className="nav nav-pills" role="tablist">
                  <li className="nav-item active">
                    <a
                      className="nav-link"
                      href="#tab1"
                      aria-controls="tab1"
                      role="tab"
                      data-toggle="tab"
                    >
                      <i className="fa fa-star" aria-hidden="true" />
                    </a>
                  </li>
                  <li className="nav-item">
                    <a
                      className="nav-link"
                      href="#tab2"
                      aria-controls="tab2"
                      role="tab"
                      data-toggle="tab"
                    >
                      <i className="fa fa-inr" aria-hidden="true" />
                      <i className="fa fa-arrow-up" aria-hidden="true" />
                    </a>
                  </li>
                  <li className="nav-item">
                    <a
                      className="nav-link"
                      href="#tab3"
                      aria-controls="tab3"
                      role="tab"
                      data-toggle="tab"
                    >
                      <i className="fa fa-inr" aria-hidden="true" />
                      <i className="fa fa-arrow-down" aria-hidden="true" />
                    </a>
                  </li>
                </ul>
              </div>
              <h5 className="pull-right">Sort By : </h5>
            </div>

            <br />
            <div className="col-md-4">
              <div className="thumbnail">
                <div className="img-thumb">
                  <img
                    className="img-reposive"
                    src="http://via.placeholder.com/350x250"
                    alt="product_image"
                  />
                </div>
                <div className="caption">
                  <p className="elipse-product">
                    <Link to="/Pro_details">de Finibus Bonorum</Link>
                  </p>
                  <button className="pull-right btn btn-danger btn-xs">
                    Add To Cart
                  </button>
                  <p>
                    <strong>20000 $</strong>
                  </p>
                  <fieldset className="rating">
                    <div className="text-warning text-center">
                      <i className="fa fa-lg fa-star" />
                      <i className="fa fa-lg fa-star" />
                      <i className="fa fa-lg fa-star" />
                      <i className="fa fa-lg fa-star-o" />
                      <i className="fa fa-lg fa-star-o" />
                    </div>
                  </fieldset>
                </div>
              </div>
            </div>
            <div className="col-md-4">
              <div className="thumbnail">
                <div className="img-thumb">
                  <img
                    className="img-reposive"
                    src="http://via.placeholder.com/350x250"
                    alt="product_image"
                  />
                </div>
                <div className="caption">
                  <p className="elipse-product">
                    <Link to="/Pro_details">de Finibus Bonorum</Link>
                  </p>
                  <button className="pull-right btn btn-danger btn-xs">
                    Add To Cart
                  </button>
                  <p>
                    <strong>20000 $</strong>
                  </p>
                  <fieldset className="rating">
                    <div className="text-warning text-center">
                      <i className="fa fa-lg fa-star" />
                      <i className="fa fa-lg fa-star" />
                      <i className="fa fa-lg fa-star" />
                      <i className="fa fa-lg fa-star-o" />
                      <i className="fa fa-lg fa-star-o" />
                    </div>
                  </fieldset>
                </div>
              </div>
            </div>
            <div className="col-md-4">
              <div className="thumbnail">
                <div className="img-thumb">
                  <img
                    className="img-reposive"
                    src="http://via.placeholder.com/350x250"
                    alt="product_image"
                  />
                </div>
                <div className="caption">
                  <p className="elipse-product">
                    <Link to="/Pro_details">de Finibus Bonorum</Link>
                  </p>
                  <button className="pull-right btn btn-danger btn-xs">
                    Add To Cart
                  </button>
                  <p>
                    <strong>20000 $</strong>
                  </p>
                  <fieldset className="rating">
                    <div className="text-warning text-center">
                      <i className="fa fa-lg fa-star" />
                      <i className="fa fa-lg fa-star" />
                      <i className="fa fa-lg fa-star" />
                      <i className="fa fa-lg fa-star-o" />
                      <i className="fa fa-lg fa-star-o" />
                    </div>
                  </fieldset>
                </div>
              </div>
            </div>
            <div className="col-md-4">
              <div className="thumbnail">
                <div className="img-thumb">
                  <img
                    className="img-reposive"
                    src="http://via.placeholder.com/350x250"
                    alt="product_image"
                  />
                </div>
                <div className="caption">
                  <p className="elipse-product">
                    <Link to="/Pro_details">de Finibus Bonorum</Link>
                  </p>
                  <button className="pull-right btn btn-danger btn-xs">
                    Add To Cart
                  </button>
                  <p>
                    <strong>20000 $</strong>
                  </p>
                  <fieldset className="rating">
                    <div className="text-warning text-center">
                      <i className="fa fa-lg fa-star" />
                      <i className="fa fa-lg fa-star" />
                      <i className="fa fa-lg fa-star" />
                      <i className="fa fa-lg fa-star-o" />
                      <i className="fa fa-lg fa-star-o" />
                    </div>
                  </fieldset>
                </div>
              </div>
            </div>
            <div className="col-md-4">
              <div className="thumbnail">
                <div className="img-thumb">
                  <img
                    className="img-reposive"
                    src="http://via.placeholder.com/350x250"
                    alt="product_image"
                  />
                </div>
                <div className="caption">
                  <p className="elipse-product">
                    <Link to="/Pro_details">de Finibus Bonorum</Link>
                  </p>
                  <button className="pull-right btn btn-danger btn-xs">
                    Add To Cart
                  </button>
                  <p>
                    <strong>20000 $</strong>
                  </p>
                  <fieldset className="rating">
                    <div className="text-warning text-center">
                      <i className="fa fa-lg fa-star" />
                      <i className="fa fa-lg fa-star" />
                      <i className="fa fa-lg fa-star" />
                      <i className="fa fa-lg fa-star-o" />
                      <i className="fa fa-lg fa-star-o" />
                    </div>
                  </fieldset>
                </div>
              </div>
            </div>
            <div className="col-md-4">
              <div className="thumbnail">
                <div className="img-thumb">
                  <img
                    className="img-reposive"
                    src="http://via.placeholder.com/350x250"
                    alt="product_image"
                  />
                </div>
                <div className="caption">
                  <p className="elipse-product">
                    <Link to="/Pro_details">de Finibus Bonorum</Link>
                  </p>
                  <button className="pull-right btn btn-danger btn-xs">
                    Add To Cart
                  </button>
                  <p>
                    <strong>20000 $</strong>
                  </p>
                  <fieldset className="rating">
                    <div className="text-warning text-center">
                      <i className="fa fa-lg fa-star" />
                      <i className="fa fa-lg fa-star" />
                      <i className="fa fa-lg fa-star" />
                      <i className="fa fa-lg fa-star-o" />
                      <i className="fa fa-lg fa-star-o" />
                    </div>
                  </fieldset>
                </div>
              </div>
            </div>
            <div className="col-md-4">
              <div className="thumbnail">
                <div className="img-thumb">
                  <img
                    className="img-reposive"
                    src="http://via.placeholder.com/350x250"
                    alt="product_image"
                  />
                </div>
                <div className="caption">
                  <p className="elipse-product">
                    <Link to="/Pro_details">de Finibus Bonorum</Link>
                  </p>
                  <button className="pull-right btn btn-danger btn-xs">
                    Add To Cart
                  </button>
                  <p>
                    <strong>20000 $</strong>
                  </p>
                  <fieldset className="rating">
                    <div className="text-warning text-center">
                      <i className="fa fa-lg fa-star" />
                      <i className="fa fa-lg fa-star" />
                      <i className="fa fa-lg fa-star" />
                      <i className="fa fa-lg fa-star-o" />
                      <i className="fa fa-lg fa-star-o" />
                    </div>
                  </fieldset>
                </div>
              </div>
            </div>
            <div className="col-md-4">
              <div className="thumbnail">
                <div className="img-thumb">
                  <img
                    className="img-reposive"
                    src="http://via.placeholder.com/350x250"
                    alt="product_image"
                  />
                </div>
                <div className="caption">
                  <p className="elipse-product">
                    <Link to="/Pro_details">de Finibus Bonorum</Link>
                  </p>
                  <button className="pull-right btn btn-danger btn-xs">
                    Add To Cart
                  </button>
                  <p>
                    <strong>20000 $</strong>
                  </p>
                  <fieldset className="rating">
                    <div className="text-warning text-center">
                      <i className="fa fa-lg fa-star" />
                      <i className="fa fa-lg fa-star" />
                      <i className="fa fa-lg fa-star" />
                      <i className="fa fa-lg fa-star-o" />
                      <i className="fa fa-lg fa-star-o" />
                    </div>
                  </fieldset>
                </div>
              </div>
            </div>
            <div className="col-md-4">
              <div className="thumbnail">
                <div className="img-thumb">
                  <img
                    className="img-reposive"
                    src="http://via.placeholder.com/350x250"
                    alt="product_image"
                  />
                </div>
                <div className="caption">
                  <p className="elipse-product">
                    <Link to="/Pro_details">de Finibus Bonorum</Link>
                  </p>
                  <button className="pull-right btn btn-danger btn-xs">
                    Add To Cart
                  </button>
                  <p>
                    <strong>20000 $</strong>
                  </p>
                  <fieldset className="rating">
                    <div className="text-warning text-center">
                      <i className="fa fa-lg fa-star" />
                      <i className="fa fa-lg fa-star" />
                      <i className="fa fa-lg fa-star" />
                      <i className="fa fa-lg fa-star-o" />
                      <i className="fa fa-lg fa-star-o" />
                    </div>
                  </fieldset>
                </div>
              </div>
            </div>
            <div className="col-md-4">
              <div className="thumbnail">
                <div className="img-thumb">
                  <img
                    className="img-reposive"
                    src="http://via.placeholder.com/350x250"
                    alt="product_image"
                  />
                </div>
                <div className="caption">
                  <p className="elipse-product">
                    <Link to="/Pro_details">de Finibus Bonorum</Link>
                  </p>
                  <button className="pull-right btn btn-danger btn-xs">
                    Add To Cart
                  </button>
                  <p>
                    <strong>20000 $</strong>
                  </p>
                  <fieldset className="rating">
                    <div className="text-warning text-center">
                      <i className="fa fa-lg fa-star" />
                      <i className="fa fa-lg fa-star" />
                      <i className="fa fa-lg fa-star" />
                      <i className="fa fa-lg fa-star-o" />
                      <i className="fa fa-lg fa-star-o" />
                    </div>
                  </fieldset>
                </div>
              </div>
            </div>
            <div className="col-md-4">
              <div className="thumbnail">
                <div className="img-thumb">
                  <img
                    className="img-reposive"
                    src="http://via.placeholder.com/350x250"
                    alt="product_image"
                  />
                </div>
                <div className="caption">
                  <p className="elipse-product">
                    <Link to="/Pro_details">de Finibus Bonorum</Link>
                  </p>
                  <button className="pull-right btn btn-danger btn-xs">
                    Add To Cart
                  </button>
                  <p>
                    <strong>20000 $</strong>
                  </p>
                  <fieldset className="rating">
                    <div className="text-warning text-center">
                      <i className="fa fa-lg fa-star" />
                      <i className="fa fa-lg fa-star" />
                      <i className="fa fa-lg fa-star" />
                      <i className="fa fa-lg fa-star-o" />
                      <i className="fa fa-lg fa-star-o" />
                    </div>
                  </fieldset>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default List;
