import React, { Component } from "react";
import "./edit_profile.css";
import { Router, Link } from "@reach/router";
class Edit extends Component {
  constructor(props: any) {
    super(props);
  }
  render() {
    return (
      <div className="container">
        <div className="container">
          <div className="container-fluid">
            <div className="col-md-12">
              <h4> My Account </h4>
              <hr />
            </div>

            <div className="col-md-3">
              <img
                src="http://soeasyloansonline.com.au/img/testimonial/noimg.png"
                className="img-responsive img-circle profile"
                alt="profile_pic"
              />

              <div className="text-center">
                <br />
                <div className="text-danger">
                  <strong>Test User</strong>
                </div>
              </div>
              <br />
              <ul className="nav">
                <li>
                  <a href="./orders.html">
                    <i className="fa fa-pencil fa-fw" aria-hidden="true" />
                    &nbsp; Orders
                  </a>
                </li>
                <li>
                  <a href="./profile.html">
                    <i className="fa fa-user fa-fw" aria-hidden="true" />
                    &nbsp; Profile
                  </a>
                </li>
                <li>
                  <a href="./address.html">
                    <i
                      className="fa fa-address-book fa-fw"
                      aria-hidden="true"
                    />
                    &nbsp; Address
                  </a>
                </li>
              </ul>
            </div>

            <div className="col-md-9">
              <div className="panel panel-default">
                <div className="panel-body">
                  <h4>
                    <strong>Edit Profile</strong>
                  </h4>
                  <hr />

                  <form>
                    <div className="row">
                      <div className="col-md-6">
                        <div className="form-group">
                          <label>First Name</label>
                          <input
                            type="text"
                            className="form-control"
                            value="Test"
                          />
                        </div>
                        <small className="text-danger">
                          Please enter valid first name
                        </small>
                      </div>
                    </div>

                    <div className="row">
                      <div className="col-md-6">
                        <div className="form-group">
                          <label>Last Name</label>
                          <input
                            type="text"
                            className="form-control"
                            value="User"
                          />
                        </div>
                        <small className="text-danger">
                          Please enter valid last name
                        </small>
                      </div>
                    </div>

                    <div className="row">
                      <div className="col-md-6">
                        <div className="form-group">
                          <label>Gender</label>
                          <div className="checkbox">
                            <label>
                              <input
                                type="radio"
                                value="male"
                                name="gender"
                                checked
                              />{" "}
                              <strong>Male</strong>{" "}
                            </label>
                            <label>
                              <input
                                type="radio"
                                value="female"
                                name="gender"
                              />{" "}
                              <strong>Female</strong>{" "}
                            </label>
                          </div>
                        </div>
                        <small className="text-danger">
                          Please select gender
                        </small>
                      </div>
                    </div>

                    <div className="row">
                      <div className="col-md-6">
                        <div className="form-group">
                          <label>Date of Birth</label>
                          <input
                            type="date"
                            className="form-control"
                            value="1993-03-15"
                          />
                        </div>
                        <small className="text-danger">
                          Please enter valid date
                        </small>
                      </div>
                    </div>

                    <div className="row">
                      <div className="col-md-6">
                        <div className="form-group">
                          <label>Mobile</label>
                          <input
                            type="number"
                            className="form-control"
                            value="1234567890"
                          />
                        </div>
                        <small className="text-danger">
                          Please enter valid mobile number
                        </small>
                      </div>
                    </div>

                    <div className="row">
                      <div className="col-md-6">
                        <div className="form-group">
                          <label>Email</label>
                          <input
                            type="email"
                            className="form-control"
                            value="test.user@neosofttech.com"
                          />
                        </div>
                        <small className="text-danger">
                          Please enter valid email
                        </small>
                      </div>
                    </div>

                    <hr />
                    <button type="submit" className="btn btn-default btn-lg">
                      <i className="fa fa-floppy-o" />
                      Save
                    </button>
                    <a
                      href="./profile.html"
                      type="button"
                      className="btn btn-default btn-lg"
                    >
                      <i className="fa fa-remove" />
                      Cancel
                    </a>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Edit;
