import React, { Component } from "react";
import "./Profile.css";
import { Router, Link } from "@reach/router";
class Profile extends Component {
  constructor(props: any) {
    super(props);
  }
  render() {
    return (
      <div className="container">
        <div className="container-fluid">
          <div className="col-md-12">
            <h4> My Account </h4>
            <hr />
          </div>

          <div className="col-md-3">
            <img
              src="http://soeasyloansonline.com.au/img/testimonial/noimg.png"
              className="img-responsive img-circle profile"
              alt="profile_pic"
            />

            <div className="text-center">
              <br />
              <div className="text-danger">
                <strong>Test User</strong>
              </div>
            </div>
            <br />
            <ul className="nav">
              <li>
                <Link to="/Orders">
                  <i className="fa fa-pencil fa-fw" aria-hidden="true" />
                  &nbsp; Orders
                </Link>
              </li>
              <li>
                <Link to="#">
                  <i className="fa fa-user fa-fw" aria-hidden="true" />
                  &nbsp; Profile
                </Link>
              </li>
              <li>
                <Link to="/Address">
                  <i className="fa fa-address-book fa-fw" aria-hidden="true" />
                  &nbsp; Address
                </Link>
              </li>
            </ul>
          </div>

          <div className="col-md-9">
            <div className="panel panel-default">
              <div className="panel-heading">
                <h2>Profile</h2>
                <hr />
              </div>
              <div className="panel-body">
                <table className="table">
                  <tbody>
                    <tr>
                      <th>First Name</th>
                      <td>Test</td>
                    </tr>
                    <tr>
                      <th>Last Name</th>
                      <td>User</td>
                    </tr>
                    <tr>
                      <th>Gender</th>
                      <td>Male</td>
                    </tr>
                    <tr>
                      <th>Date of Birth</th>
                      <td>15/03/1993</td>
                    </tr>
                    <tr>
                      <th>Mobile Number</th>
                      <td>1234567890</td>
                    </tr>
                    <tr>
                      <th>Email</th>
                      <td>test.user@neosofttech.com</td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div className="panel-footer">
                <hr />
                <Link
                  to="/edit_pro"
                  type="button"
                  className="btn btn-default btn-lg"
                >
                  Edit
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Profile;
