import React, { Component } from "react";
import "./App.css";
import Header from "./Components/Header/header";
import Corausel from "./Screens/Corausel/Corausel";
import Footer from "./Components/Foot/Footer";
import { Router, Link } from "@reach/router";
import List from "./Screens/Product_list/pro_list";
import Cart from "./Screens/Cart/cart";
import Details from "./Screens/Product_list/pro_details";
import Profile from "./Screens/Profile/Profile";
import Orders from "./Screens/Orders/Orders";
import Address from "./Screens/Address/Address";
import Login from "./Screens/Login/Login";
import Register from "./Screens/Register/Register";
import Del_Add from "./Screens/Checkout/address";
import Edit from "./Screens/Profile/edit_profile";
import Terms from "./Screens/Information/Terms";
import Guarantee from "./Screens/Information/Guarantee";
import Contact from "./Screens/Information/Contact";
import Privacy from "./Screens/Information/Privacy";
import Locate from "./Screens/Information/Locate";
import EditAdd from "./Screens/Address/Edit_add";
class App extends Component {
  render() {
    return (
      <div className="App">
        <Header />
        <br />
        <Router>
          <Corausel path="/" />
          <List path="Product_list" />
          <Cart path="cart" />
          <Details path="Pro_details" />
          <Profile path="Profile" />
          <Orders path="Orders" />
          <Address path="Address" />
          <Login path="Login" />
          <Register path="Register" />
          <Del_Add path="del_address" />
          <Edit path="edit_pro" />
          <Terms path="terms" />
          <Guarantee path="guarantee" />
          <Contact path="contact" />
          <Privacy path="privacy" />
          <Locate path="locate_us" />
          <EditAdd path="edit_add" />
        </Router>
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <Footer />
        <br />
      </div>
    );
  }
}

export default App;
