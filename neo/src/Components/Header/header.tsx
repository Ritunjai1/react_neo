import React, { Component } from "react";
import "./header.css";
import { Router, Link } from "@reach/router";

interface IProps {
  autocomplete: true;
}
class Header extends Component {
  constructor(props: any) {
    super(props);
  }
  public render() {
    return (
      <div>
        {/* <!-- Header --> */}
        <nav className="navbar navbar-default">
          <div className="container">
            {/* <!-- Brand and toggle get grouped for better mobile display --> */}
            <div className="navbar-header">
              <button
                type="button"
                className="navbar-toggle collapsed"
                data-toggle="collapse"
                data-target="#navbar"
                aria-expanded="false"
              >
                <span className="sr-only">Toggle navigation</span>
                <span className="icon-bar" />
                <span className="icon-bar" />
                <span className="icon-bar" />
              </button>
              <Link to="/" className="navbar-brand">
                <i className="fa fa-shopping-bag fa-lg" aria-hidden="true">
                  {" "}
                  <strong>NeoSTORE</strong>{" "}
                </i>
              </Link>
            </div>

            {/* <!-- Collect the nav links, forms, and other content for toggling --> */}
            <div className="collapse navbar-collapse" id="navbar">
              <form className="pull-left">
                <div className="row">
                  <div className="col-md-12">
                    <div className="form-group">
                      <div className="input-group search-bar">
                        <input name="search" className="form-control" />
                        <span className="input-group-btn">
                          <button type="submit" className="btn btn-default">
                            <i className="fa fa-search" />
                          </button>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </form>

              <ul className="nav navbar-nav navbar-right">
                <li className="dropdown">
                  <a
                    href="#"
                    className="dropdown-toggle"
                    data-toggle="dropdown open"
                    role="button"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    <i className="fa fa-user fa-lg" area-hidden="true" />
                    <span className="caret" />
                  </a>
                  <ul className="dropdown-content">
                    <li>
                      <Link to="Profile">
                        <i className="fa fa-user fa-fw" aria-hidden="true" />
                        &nbsp; Profile
                      </Link>
                    </li>
                    <li>
                      <Link to="Orders">
                        <i
                          className="fa fa-list-alt fa-fw"
                          aria-hidden="true"
                        />
                        &nbsp; Orders
                      </Link>
                    </li>
                    <li>
                      <Link to="Address">
                        <i
                          className="fa fa-address-card-o fa-fw"
                          aria-hidden="true"
                        />
                        &nbsp; Addresses
                      </Link>
                    </li>
                    <li>
                      <a href="/">
                        <i
                          className="fa fa-sign-out fa-fw"
                          aria-hidden="true"
                        />
                        &nbsp; Logout
                      </a>
                    </li>
                    <li>
                      <Link to="Login">
                        <i className="fa fa-sign-in fa-fw" aria-hidden="true" />
                        &nbsp; Login
                      </Link>
                    </li>
                    <li>
                      <Link to="Register">
                        <i
                          className="fa fa-user-plus fa-fw"
                          aria-hidden="true"
                        />
                        &nbsp; Register
                      </Link>
                    </li>
                  </ul>
                </li>
              </ul>

              <ul className="nav navbar-nav navbar-right">
                <li>
                  <Link to="cart">
                    <i
                      className="fa fa-shopping-cart fa-lg"
                      aria-hidden="true"
                    />
                    Cart <span className="badge">3</span>
                  </Link>
                </li>
              </ul>
            </div>
            {/* /.navbar-collapse --> */}
          </div>
          {/* <!-- /.container-fluid --> */}
        </nav>
      </div>
    );
  }
}
export default Header;
